from django import forms
from django.contrib.auth.forms import UserCreationForm
 
from.models import *


class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = Students
        fields = ["username", "email", "password", "password1"]


class PostSearchForm(forms.Form):
    q = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(). __init__(*args, **kwargs)
        self.fields['q'].label = 'Search For'
        self.fields['q'].widget.attrs.update(
            {'class': 'form-control'})


class AddbookForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = {'category_name', "title", "auther",
                  "publisher", "Date_of_publication"}


class AddCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = {'name', "description"}


class IssuebookForm(forms.ModelForm):
    Std_Name = forms.CharField(max_length=255)
    Reg_No = forms.IntegerField()

    class Meta:
        model = Books

        fields = {'category_name', "title", "auther",
                  "publisher", "Date_of_publication"}


class UpdateBookForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = {'category_name', "title", "auther",
                  "publisher", "Date_of_publication"}


class Administrator(forms.ModelForm):
    class Meta:
        model = Question
        fields = {'publication_date', 'question_text'}
