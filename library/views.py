
from django.template import RequestContext
from . forms import *
from library.models import Fine
from library.models import Books
from library.forms import PostSearchForm
from django.shortcuts import render, redirect
from platformdirs import user_data_dir, user_data_path
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User, Group
from.models import *


# Create your views her
def home(request):
    return render(request, 'library/home.html', {})
def logout_user(request):
    logout(request)
    return redirect("/login")


def login(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()

            login(request, user)
            return redirect('/home')
    else:
        form = RegisterForm()
    return render(request, 'registration/login.html', {"form": form})
def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()

            login(request, user)
            return redirect('/home')
    else:
        form = RegisterForm()
    return render(request, 'registration/signup.html', {"form": form})



def post_search(request):
    form = PostSearchForm
    if request.method == "POST":

        search = request.POST.get('q')
        results = Books.objects.filter(title__icontains=search)
        print(results)

        return render(request, 'library/index.html', {'results': results, })
    else:
        form = PostSearchForm()
        return render(request, 'library/index.html', {'form': form, })

@login_required(login_url="/login")
@permission_required("Add_category", login_url="/login", raise_exception="True")
def Add_category(request):
    if request.method == 'POST':
        forms = AddCategoryForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('category_list')

    form = AddCategoryForm
    return render(request, 'library/add_category.html', {'form': form})


def category_list(request):
    all = Category.objects.all()
    return render(request, 'library/category_list.html', {'all': all})

@login_required(login_url="/login")
@permission_required("Add_booK", login_url="/login", raise_exception="True")
def Add_books(request):
    if not request.user.is_superuser:
        return redirect('available_book')
    form = AddbookForm()
    
    if request.method == 'POST':
        forms = AddbookForm(request.POST)
        print(forms.errors)
        if forms.is_valid():
            add = forms.save(commit=False)
            forms.save()

            return redirect('available_book')
    form = AddbookForm
    return render(request, 'LMS/Addbook.html', {'form': form})


def available_book(request):
    all_books = Books.objects.all()
    for i in all_books:
        print(i)
    return render(request, 'LMS/available_book.html', {'all': all_books})

@login_required(login_url="/login")
@permission_required("Issue_books", login_url="/login", raise_exception="True")
def Issue_books(request):
    if not request.user.is_superuser:
        return redirect('available_book')
    if request.method == 'POST':
        forms = IssuebookForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('Issue_book')

    form = IssuebookForm
    return render(request, 'LMS/Issuebook.html', {'form': form})


@login_required(login_url="/login")
@permission_required("Add_booK", login_url="/login", raise_exception="True")
def UpdateBook(request, pk):
    if not request.user.is_superuser:
        return redirect('available_book')
    X = Books.objects.get(id = pk)
    form = UpdateBookForm(instance = X)
    if request.method == 'POST':
        forms = UpdateBookForm(request.POST ,files = request.FILES,instance = X)
        if forms.is_valid():
            X = forms.save(commit = False)

            return redirect('available_book')
    form = UpdateBookForm
    return render(request, 'LMS/update_book.html', {'form': form})
