

from library import views
from django.urls import path
from.import views

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('signup', views.signup, name='signup'),
    path('login', views.login, name='login'),
    
    path('post-search/', views.post_search, name='post-search'),
    path('Add_book', views.Add_books, name='Add_book'),
    path('Add_category', views.Add_category, name='Add_category'),
    path('category_list', views.category_list, name='category_list'),
    path('Issue_book', views.Issue_books, name='Issue_book'),
    path('Update/<int:pk>', views.UpdateBook, name='Update'),
    path('available_book', views.available_book, name='available_book')
]
