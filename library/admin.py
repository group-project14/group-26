from django.contrib import admin

# Register your models here.
from.models import Question, Students, Books, Fine, Borrowed_books,Category


admin.site.register(Students)
admin.site.register(Books)
admin.site.register(Fine)
admin.site.register(Borrowed_books)
admin.site.register(Category)


class QuestionAdmin(admin.ModelAdmin):

    fields = ['publication_date', 'question_text']


admin.site.register(Question, QuestionAdmin)
