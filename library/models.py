from django.db import models
from django.contrib.auth.models import AbstractUser
from django.shortcuts import render
from django.utils.translation import gettext as text
from django.core.exceptions import PermissionDenied


# Create your models here.


class Students(AbstractUser):
    GENDER_TYPE = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    borrowed_at = models.DateTimeField(auto_now_add=True)
    returned_at = models.DateTimeField(auto_now_add=True)
    contact = models.CharField(max_length=255, blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER_TYPE)


def __str__(self):
    return self.first_name + "\n" + self.last_name


class Category(models.Model):
    name = models.CharField(max_length=255, null=False, db_index=True)
    description = models.CharField(max_length=255, null=False, db_index=True)

    def __str__(self):
        return self.name


class Books(models.Model):
    category_name = models.ForeignKey(
        Category, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField("title", max_length=255,
                             null=False, db_index=True, default="tuyey")
    auther = models.CharField("auther", max_length=255, null=True, blank=True)
    Date_of_publication = models.CharField(
        max_length=255, blank=True, null=True)
    publisher = models.CharField(
        "publisher", max_length=68, null=True, blank=True)

    def __str__(self):
        return self.title


class Fine(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    Amount = models.IntegerField(null=True)
    Return_date = models.DateTimeField(auto_now_add=True)
    Number_of_days_delayed = models.IntegerField(blank=True)

    def __str__(self):
        return self.Amount


class Borrowed_books(models.Model):
    Std_No = models.ForeignKey(Students, on_delete=models.CASCADE)
    title = models.CharField("title", max_length=255,
                             null=False, db_index=True)
    auther = models.CharField("auther", max_length=255)
    Edition = models.CharField(max_length=300)
    Date_to_be_returned = models.DateTimeField(auto_now_add=True)
    Time_given_out = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Question(models.Model):
    question_text = models.CharField(max_length=255, null=False, db_index=True)
    publication_date = models.DateTimeField()


# class AdvUser(AbstractUser):
#     is_activated = models.BooleanField(default=True, db_index=True,
#                                        verbose_name='Пpoшeл активацию?')
#     send_messages = models.BooleanField(default=True, verbose_name='Слать оповещения о новых комментариях?')

# class Meta(AbstractUser.Meta):
#         pass


# user_registrated = Signal(providing_args=['instance'])


# def user_registrated_dispatcher(sender, **kwargs):
#     send_activation_notification(kwargs['instance'])


# user_registrated.connect(user_registrated_dispatcher)
